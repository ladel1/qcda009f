package tests;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

import bo.Personne;
import demoInterface.MyInterface;

public class Main {

	public static void main(String[] args) {
		List<Personne> personnes = new ArrayList<Personne>();
		personnes.add(new Personne("Duval","Sarah", 15));
		personnes.add(new Personne("Maisel","Lucas", 26));
		personnes.add(new Personne("Duval","Gaelle", 30));
		personnes.add(new Personne("Maisel","Hugo", 17));
		personnes.add(new Personne("Duval","steve", 30));
		personnes.add(new Personne("Maisel","Pierre", 5));

		
//		personnes.sort(new Comparator<Personne>() {
//
//			@Override
//			public int compare(Personne o1, Personne o2) {
//				// TODO Auto-generated method stub
//				return o1.getPrenom().compareTo(o2.getPrenom());
//			}
//			
//		});
		
		personnes.sort((o1, o2) -> o1.getPrenom().compareTo(o2.getPrenom()) );
		
		personnes.forEach(p -> System.out.println(p.getPrenom().toUpperCase()));
		
		System.out.println("************** Variablisée l'expression lambda ***************");
		// Variablisée l'expression lambda
		MyInterface m = msg->  System.out.println("Salut %s".formatted(msg)); 		
		m.maMethode("Gaelle");
		System.out.println("************** API Stream ***************");
		
		List<Personne> personneSup18Ans = personnes.stream()
												   .filter( p->p.getAge()>=18 )
												   .collect( Collectors.toList() );

		// références de methode
		personneSup18Ans.forEach(System.out::println);
		
		OptionalDouble average = personnes.stream()
		         .mapToInt( p-> p.getAge() )		       
		         .average();
		
		if (average.isPresent()) {			
			System.out.println("La moyenne d'age %f ".formatted( average.getAsDouble() ));
		}
		
		
		System.out.println("********** Optional *********** ");
		Optional<Personne> optPersonne = Optional.ofNullable(new Personne("Maisel","Pierre", 5));
		
		optPersonne.ifPresent(p->System.out.println(p.getNom()));
		optPersonne.ifPresentOrElse(System.out::println,
				()->System.out.println("L'objet personne est null"));
				   
		
//		if(optPersonne.isPresent()) {
//			System.out.println(optPersonne.get().getNom());
//		}
//		
//		if(optPersonne.isEmpty()) {
//			System.err.println("Personne est null");
//		}
		
	}

}
