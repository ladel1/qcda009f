package fr.eni.ecole.bo;

import java.io.Serializable;
import java.util.List;

public record Personnes(List<Personne> personnes) implements Serializable {

}
