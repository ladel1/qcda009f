package fr.eni.ecole.bo;

import java.io.Serializable;

public class Personne implements Serializable {

	private String nom;
	private String prenom;
	private int age;
	public Personne(String nom, String prenom, int age) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
	}
	
	public void blabalb() {
		
	}
	
	@Override
	public String toString() {
		return "Personne [nom=" + nom + ", prenom=" + prenom + ", age=" + age + "]";
	}
}
