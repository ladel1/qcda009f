package fr.eni.ecole.bo;

import java.io.Serializable;

public class Article implements Serializable {

	private String nom;
	private String description;
	private float prix;
	
	public Article(String nom, String description, float prix) {
		this.nom = nom;
		this.description = description;
		this.prix = prix;
	}
	
	@Override
	public String toString() {
		return "Article [nom=" + nom + ", description=" + description + ", prix=" + prix + "]";
	}
	
	
	
}
