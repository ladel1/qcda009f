package fr.eni.ecole.test;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import fr.eni.ecole.bo.Article;
import fr.eni.ecole.bo.Personne;
import fr.eni.ecole.bo.Personnes;

public class Main {

	private static void serialiser(String filename,Object o) {
		// Try with resources
		try(
			FileOutputStream file = new FileOutputStream(filename);
			ObjectOutputStream out = new ObjectOutputStream(file);					
			){			
			out.writeObject(o);
			System.out.println("L'objet est sérialisé avec succès 😊");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	private static Object deserialiser(String filename) {
		// Try with resources
		try(
			FileInputStream file = new FileInputStream(filename);
			ObjectInputStream out = new ObjectInputStream(file);					
			){			
			return out.readObject();
		} catch (IOException | ClassNotFoundException e) {			
			e.printStackTrace();
			return null;
		}
	}
	
	public static void main(String[] args) {
		//Personnes personnes = new Personnes(List.of(new Personne("De lor", "Hugo",26),new Personne("Maisel","Eric",55)));
		String filename = "personnes.ser";
		//serialiser(filename,personnes);
		Object o = deserialiser(filename);
		if(o!=null && o instanceof Personnes) {
			
			Personnes personne = (Personnes) o;
			
			System.out.println(personne);
		}
		
//		Article samsung = new Article("Samsung S21 PRO MAX EXTRA ULTRA", "blabalbla", 1200);
//		serialiser("samsung.ser", samsung);
		
//		Object o = deserialiser("samsung.ser");
//		if(o!=null && o instanceof Article) {
//			
//			Article samsung = (Article) o;
//			
//			System.out.println(samsung);
//		}
		
	}
}
