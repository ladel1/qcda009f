package tools;

import java.io.Serializable;
import java.util.Arrays;

public class MyListCreator<T extends Serializable> {

	private int size = 0;
	private Object [] tab;
	
	public MyListCreator() {
		this.tab = new Object[0];
	}
	
	public void add( T ele ) {
		size++;
		Object [] listeTemp = new Object[size];
		
		for (int i = 0; i < tab.length; i++) {
			listeTemp[i] = tab[i];
		}		
		listeTemp[size-1] = ele;		
		tab = listeTemp;		
	}
	
	public void save() {
		
	}
	
	
	@Override
	public String toString() {
		return Arrays.toString(tab);
	}
	
}
