package bo;

import java.io.Serializable;

public class Personne implements Serializable{

	private String nom;
	private String prenom;
	public Personne() {}
	public Personne(String nom, String prenom) {
		super();
		this.nom = nom;
		this.prenom = prenom;
	}
	@Override
	public String toString() {
		return "Personne [nom=" + nom + ", prenom=" + prenom + "]";
	}
	
	
	
}
