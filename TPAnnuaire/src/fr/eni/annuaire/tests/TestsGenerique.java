package fr.eni.annuaire.tests;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.eni.annuaire.bo.Adresse;
import fr.eni.annuaire.bo.Contact;
import fr.eni.annuaire.bo.Personnel;
import fr.eni.annuaire.bo.Professionnel;
import fr.eni.annuaire.bo.Utilisateur;
import fr.eni.annuaire.dal.ContactDao;
import fr.eni.annuaire.dal.ContactFactoryDao;
import fr.eni.annuaire.dal.UtilisateurDao;
import fr.eni.annuaire.dal.UtilisateurFactoryDao;
import fr.eni.annuaire.dal.tools.SerialiseurBinaire;
import fr.eni.annuaire.dal.tools.SerialiseurJson;
import fr.eni.annuaire.data.Contacts;
import fr.eni.annuaire.data.Utilisateurs;

public class TestsGenerique {

	public static void main(String[] args) {
		// PARTIE OBLIGATOIRE
		
		//1- tester l'interface générique Dao<T>
		System.out.println("Partie 1 - interface générique.");
		UtilisateurDao utilisateurDao = UtilisateurFactoryDao.createInstance("binary"); 
		List<Utilisateur> utilisateursTrouves = utilisateurDao.selectAll();
		System.out.printf("==> Utilisateurs trouvés : %s%n", utilisateursTrouves.toString());
		ContactDao contactDao = ContactFactoryDao.createInstance("binary"); 
		
//		contactDao.insert(new Contact(1, "Steve", "03165464", "Steve@gmail.com", 
//				                      new Adresse(0, "", "79000", "Niort"))
//				);
		
		List<Contact> contactsTrouves = contactDao.selectAll();
		System.out.printf("==> Contacts trouvés : %s%n", contactsTrouves.toString());
	
		
		//2- tester la serialisation/déserialisation binaire en s'appuyant sur les méthodes génériques
		System.out.println("Partie 2 - serialisation/déserialisation binaire à l'aide des méthodes génériques.");
		//partie utilisateur
		List<Utilisateur> tempUsers = List.of( 
				new Utilisateur(1, "Ju", "Julie", "Pa$$w0rd"),
				new Utilisateur(2, "Cha", "Charline", "Pa$$w0rd"),
				new Utilisateur(3, "Jus", "Justin", "Pa$$w0rd"));
		Utilisateurs usersContainer = new Utilisateurs(tempUsers);
		SerialiseurBinaire.exporterToBinary("storage/utilisateurs.bin", usersContainer); //vérifier que le fichier à été créé
		
		Utilisateurs restoreBinaireUsers = SerialiseurBinaire.importerFromBinary("storage/utilisateurs.bin");
		System.out.println("==> Liste des utilisateurs restaurés depuis binaire");
		for (Utilisateur utilisateur : restoreBinaireUsers.utilisateurs()) {
			System.out.println(utilisateur.toString());
		}
//		
//		//partie contact
		Adresse a1 = new Adresse(1, "9 chemin des bois", "44000", "Nantes");
		Adresse a2 = new Adresse(2, "1 rue de la forêt", "35000", "Rennes");
		Adresse a3 = new Adresse(3, "2b rue Faraday", "44800", "Saint Herblain"); 
		Contact c1 = new Personnel(1, "Tom", "0601020304", "tom@campus-eni.fr", a1, LocalDate.of(2000, 9, 22));
		Contact c2 = new Personnel(2, "Felix", "0605060708", "felix@campus-eni.fr", a2, LocalDate.of(2005, 5, 10));
		Contact c3 = new Professionnel(3, "Samuel", "0609101112", "samuel@campus-eni.fr", a3, "ENI Ecole");
		Contact c4 = new Professionnel(4, "Jade", "0613141516", "jade@campus-eni.fr", a3, "ENI Ecole");
		Contact c5 = new Professionnel(5, "Lauryne", "0617181920", "charline@campus-eni.fr", null, "Xpert");
		List<Contact> tempContacts = new ArrayList<>();
		tempContacts.add(c1);
		tempContacts.add(c2);
		tempContacts.add(c3);
		tempContacts.add(c4);		
		System.err.println(tempContacts);
		Contacts contactsContainer = new Contacts(tempContacts);
		
		SerialiseurBinaire.exporterToBinary("storage/contacts.bin", contactsContainer); //vérifier que le fichier à été créé
		
		Contacts restoreBinaireContacts = SerialiseurBinaire.importerFromBinary("storage/contacts.bin");
		System.out.println("==> Liste des contacts restaurés depuis binaire");
		for (Contact contact : restoreBinaireContacts.contacts()) {
			System.out.println(contact.toString());
		}
		
		// PARTIE OPTIONNELLE
		//3- tester la serialisation/déserialisation json en s'appuyant sur les méthodes génériques
		//partie utilisateur
		
		SerialiseurJson.exporterToJson("storage/utilisateurs.json", usersContainer); //vérifier que le fichier à été créé
		
		Utilisateurs restoreJsonUsers = SerialiseurJson.importerFromJson("storage/utilisateurs.json", Utilisateurs.class);
		System.out.println("==> Liste des utilisateurs restaurés depuis json");
		for (Utilisateur utilisateur : restoreJsonUsers.utilisateurs()) {
			System.out.println(utilisateur.toString());
		}
		//partie contact
		SerialiseurJson.exporterToJson("storage/contacts.json", contactsContainer); //vérifier que le fichier à été créé
		
		Contacts restoreJsonContacts = SerialiseurJson.importerFromJson("storage/contacts.json", Contacts.class);
		System.out.println("Liste des contacts restaurés depuis json");
		for (Contact contact : restoreJsonContacts.contacts()) {
			System.out.println(contact.toString());
		}
	}

}
