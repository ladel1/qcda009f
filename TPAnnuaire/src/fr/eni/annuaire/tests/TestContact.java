package fr.eni.annuaire.tests;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.eni.annuaire.bo.Adresse;
import fr.eni.annuaire.bo.Contact;
import fr.eni.annuaire.bo.Personnel;
import fr.eni.annuaire.bo.Professionnel;
import fr.eni.annuaire.dal.tools.SerialiseurBinaire;
import fr.eni.annuaire.dal.tools.SerialiseurJson;
import fr.eni.annuaire.data.Contacts;

public class TestContact {

	public static void main(String[] args) {
		Adresse a1 = new Adresse(1, "9 chemin des bois", "44000", "Nantes");
		Adresse a2 = new Adresse(2, "1 rue de la forêt", "35000", "Rennes");
		// TODO Auto-generated method stub
		Personnel c4 = new Personnel(1, "Tom", "0601020304", "tom@campus-eni.fr", a1, LocalDate.of(2000, 9, 22));
		Personnel c5 = new Personnel(2, "Felix", "0605060708", "felix@campus-eni.fr", a2, LocalDate.of(2005, 5, 10));
		List<Personnel> tempContacts = new ArrayList<>();
		tempContacts.add(c5);
		tempContacts.add(c5);		
		SerialiseurJson.exporterToJson("storage/contacts.json", tempContacts);
	}

}
