package fr.eni.annuaire.dal;

import java.util.List;

import fr.eni.annuaire.bo.Contact;

public interface ContactDao extends Dao<Contact> {
	// CRUD Contact
	List<Contact> selectByNom(String nom);
}
