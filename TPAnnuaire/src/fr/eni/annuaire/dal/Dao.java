package fr.eni.annuaire.dal;

import java.util.List;

public interface Dao<T> {

	void insert(T newElement);
	void update(T newElement);
	void delete(long id);
	List<T> selectAll();
	
}
