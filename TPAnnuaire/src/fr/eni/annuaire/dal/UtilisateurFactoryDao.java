package fr.eni.annuaire.dal;

import fr.eni.annuaire.dal.impl.UtilisateurBinaryImplDao;
import fr.eni.annuaire.dal.impl.UtilisateurJsonImplDao;

public abstract class UtilisateurFactoryDao {	
	public static UtilisateurDao createInstance(String type) {
		if(type.equalsIgnoreCase("Binary")) return new UtilisateurBinaryImplDao();
		if(type.equalsIgnoreCase("JSON")) return new UtilisateurJsonImplDao();		
		return null;
	}
	
}
