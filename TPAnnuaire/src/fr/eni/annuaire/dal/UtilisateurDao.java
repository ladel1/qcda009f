package fr.eni.annuaire.dal;

import fr.eni.annuaire.bo.Utilisateur;

public interface UtilisateurDao extends Dao<Utilisateur> {
	// CRUD Utilisateur
	Utilisateur selectBy(String pseudo,String motPasse);
}
