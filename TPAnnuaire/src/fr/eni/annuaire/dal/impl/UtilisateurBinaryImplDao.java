package fr.eni.annuaire.dal.impl;

import java.util.List;

import fr.eni.annuaire.bo.Utilisateur;
import fr.eni.annuaire.dal.UtilisateurDao;
import fr.eni.annuaire.dal.tools.SerialiseurBinaire;
import fr.eni.annuaire.data.Utilisateurs;

public class UtilisateurBinaryImplDao implements UtilisateurDao {

	private Utilisateurs utilisateurs;
	
	public UtilisateurBinaryImplDao() {
		this.utilisateurs = (Utilisateurs) SerialiseurBinaire.importerFromBinary("storage/utilisateurs.bin");
	}
	
	@Override
	public Utilisateur selectBy(String pseudo, String motPasse) {		
		for (Utilisateur ele : utilisateurs.utilisateurs()) {
			if(ele.getPseudo().equals(pseudo) && ele.getMotPasse().equals(motPasse)) {
				return ele;
			}
		}		
		return null;
	}

	@Override
	public void insert(Utilisateur newElement) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Utilisateur newElement) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Utilisateur> selectAll() {		
		return utilisateurs.utilisateurs();
	}

	
	
}
