package fr.eni.annuaire.dal.impl;

import java.util.ArrayList;
import java.util.List;

import fr.eni.annuaire.bo.Contact;
import fr.eni.annuaire.dal.ContactDao;
import fr.eni.annuaire.dal.tools.SerialiseurBinaire;
import fr.eni.annuaire.data.Contacts;

public class ContactBinaryImplDao implements ContactDao {

	private Contacts contacts;
	private final static String FILE_NAME = "storage/contacts.ser";
	
	public ContactBinaryImplDao() {
		contacts = (Contacts) SerialiseurBinaire.importerFromBinary(FILE_NAME);
		if(contacts==null) {
			contacts = new Contacts(new ArrayList<Contact>());
		}
	}
	
	@Override
	public void insert(Contact newElement) {
		contacts.contacts().add(newElement);
		SerialiseurBinaire.exporterToBinary(FILE_NAME, contacts);
	}

	@Override
	public void update(Contact newElement) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Contact> selectAll() {
		return contacts.contacts();
	}

	@Override
	public List<Contact> selectByNom(String nom) {
		return null;
	}

}
