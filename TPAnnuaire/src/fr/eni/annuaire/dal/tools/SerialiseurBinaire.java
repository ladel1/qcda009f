package fr.eni.annuaire.dal.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import fr.eni.annuaire.data.Contacts;
import fr.eni.annuaire.data.Utilisateurs;

public class SerialiseurBinaire {

	
	public static <T> void exporterToBinary(String filename, T elements) {
		// Try with resources
		try(
			FileOutputStream file = new FileOutputStream(filename);
			ObjectOutputStream out = new ObjectOutputStream(file);					
			){			
			out.writeObject(elements);
			System.out.println("L'objet est sérialisé avec succès 😊");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	public static <T> T importerFromBinary(String filename) {
		File fileExists = new File(filename);
		if(!fileExists.exists()) return null;
		// Try with resources
		try(
		
			FileInputStream file = new FileInputStream(filename);
			ObjectInputStream out = new ObjectInputStream(file);					
			){			
			return (T) out.readObject();
		} catch (IOException | ClassNotFoundException e) {			
			e.printStackTrace();
			return null;
		}
	}

}
