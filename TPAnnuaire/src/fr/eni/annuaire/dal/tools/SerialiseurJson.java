package fr.eni.annuaire.dal.tools;

import java.io.File;
import java.io.IOException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;


public class SerialiseurJson {

	
	public static <T> void exporterToJson(String filename,T elements) {
		// Try without resources
		try{
			File file = new File(filename);
			ObjectMapper objectMapper = new ObjectMapper();	
			objectMapper.registerModule(new JavaTimeModule());
			objectMapper.writeValue(file, elements);
			System.out.println("L'objet est sérialisé avec succès 😊 JSON");			
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	public static <T> T importerFromJson(String filename, Class<T> classe) {
		// Try without resources
		try{	
			ObjectMapper objectMapper = new ObjectMapper();	
			objectMapper.registerModule(new JavaTimeModule());
			return  (T) objectMapper.readValue(new File(filename),classe);
		} catch (IOException  e) {			
			e.printStackTrace();
			return null;
		}
	}
	
}
