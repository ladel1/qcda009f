package fr.eni.annuaire.data;

import java.io.Serializable;
import java.util.List;

import fr.eni.annuaire.bo.Contact;

public record Contacts(List<Contact> contacts) implements Serializable {
}
