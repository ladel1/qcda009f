package fr.eni.annuaire.data;

import java.io.Serializable;
import java.util.List;

import fr.eni.annuaire.bo.Utilisateur;


public record Utilisateurs(List<Utilisateur> utilisateurs) implements Serializable {
}
