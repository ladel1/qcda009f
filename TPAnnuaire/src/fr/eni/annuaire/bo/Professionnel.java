package fr.eni.annuaire.bo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class Professionnel extends Contact implements Serializable {

	private String nomEntreprise;
	
	public Professionnel() {
		super();
	}

	public Professionnel(long numero, String nom, String telephone, String adresseMail, Adresse adresse,
			String nomEntreprise) {
		super(numero, nom, telephone, adresseMail, adresse);
		this.nomEntreprise = nomEntreprise;
	}

	@Override
	public String toString() {
		return "Professionnel [nomEntreprise=" + nomEntreprise + ", toString()=" + super.toString() + "]";
	}

	public String getNomEntreprise() {
		return nomEntreprise;
	}

	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}
	
	

//	@Override
//	protected String getNature() {
//		return getClass().getSimpleName();
//	}


	
	
	
	
	
}
