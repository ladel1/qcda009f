package fr.eni.annuaire.bo;

import java.io.Serializable;

public class Utilisateur implements Serializable {

	private int id;
	private String nom;
	private String pseudo;
	private String motPasse;
	
	public Utilisateur() {}
	
	public Utilisateur(int id,String pseudo, String nom,  String motPasse) {
		this.id = id;
		this.nom = nom;
		this.pseudo = pseudo;
		this.motPasse = motPasse;
	}


	
	
	public int getId() {
		return id;
	}




	public void setId(int id) {
		this.id = id;
	}




	public String getNom() {
		return nom;
	}




	public void setNom(String nom) {
		this.nom = nom;
	}




	public String getPseudo() {
		return pseudo;
	}




	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}




	public String getMotPasse() {
		return motPasse;
	}




	public void setMotPasse(String motPasse) {
		this.motPasse = motPasse;
	}




	@Override
	public String toString() {
		return "Personnel [id=" + id + ", nom=" + nom + ", pseudo=" + pseudo + ", motPasse=" + motPasse + "]";
	}
	
}
