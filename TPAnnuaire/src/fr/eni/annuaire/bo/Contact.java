package fr.eni.annuaire.bo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
		  use = JsonTypeInfo.Id.NAME, 
		  include = JsonTypeInfo.As.PROPERTY, 
		  property = "type")
@JsonSubTypes({ 
  @Type(value = Personnel.class, name = "Personnel"), 
  @Type(value = Professionnel.class, name = "Professionnel")
})
public class Contact implements Serializable {

	private long numero;
	private String nom;
	private String telephone;
	private String adresseMail;
	private boolean favoris;
	private Adresse adresse;
	
	public Contact() {
		// TODO Auto-generated constructor stub
	}
	
	public Contact(long numero, String nom, String telephone, String adresseMail, Adresse adresse) {
		this.numero = numero;
		this.nom = nom;
		this.telephone = telephone;
		this.adresseMail = adresseMail;
		this.adresse = adresse;
	}
	
	// abstract 
	protected String getNature() {
		return getClass().getSimpleName();
	}


	@Override
	public String toString() {
		return "Contact [numero=" + numero + ", nom=" + nom + ", telephone=" + telephone + ", adresseMail="
				+ adresseMail + ", favoris=" + favoris + ", adresse=" + adresse + ", getNature()=" + getNature() + "]";
	}

	public long getNumero() {
		return numero;
	}

	public void setNumero(long numero) {
		this.numero = numero;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getAdresseMail() {
		return adresseMail;
	}

	public void setAdresseMail(String adresseMail) {
		this.adresseMail = adresseMail;
	}

	public boolean isFavoris() {
		return favoris;
	}

	public void setFavoris(boolean favoris) {
		this.favoris = favoris;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}
	
	
	
	
}
