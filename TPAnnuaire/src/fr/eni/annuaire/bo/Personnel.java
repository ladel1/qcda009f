package fr.eni.annuaire.bo;

import java.io.Serializable;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class Personnel extends Contact implements Serializable {

	private LocalDate dateAnniversaire;
	
	public Personnel() {
		super();
	}
	
	public Personnel(long numero, String nom, String telephone, String adresseMail, Adresse adresse,
			LocalDate dateAnniversaire) {
		super(numero, nom, telephone, adresseMail, adresse);
		this.dateAnniversaire = dateAnniversaire;
	}

	@Override
	public String toString() {
		return "Personnel [dateAnniversaire=" + dateAnniversaire + ", toString()=" + super.toString() + "]";
	}

	public LocalDate getDateAnniversaire() {
		return dateAnniversaire;
	}

	public void setDateAnniversaire(LocalDate dateAnniversaire) {
		this.dateAnniversaire = dateAnniversaire;
	}

//
//	@Override
//	protected String getNature() {
//		return getClass().getSimpleName();
//	}


	
	
}
