package dInterface2;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

public class Main {

	
	public static void main(String[] args) {
		Burger bViande = new Burger("Viande");
		Burger bVegan = new Burger("Vegan");
		List.of(bViande,bVegan)
		.stream()
		.filter( burger->burger.getType().equals("Viande") )
		.forEach(System.out::println);
	}

}
