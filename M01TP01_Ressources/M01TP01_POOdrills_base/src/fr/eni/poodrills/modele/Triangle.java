package fr.eni.poodrills.modele;

public class Triangle extends Forme {
	//Attributs
	private float largeur;
	private float hauteur;
	
	
	//Constructeurs

	
	public Triangle(int largeur, int hauteur) {
		this.largeur=largeur;
		this.hauteur=hauteur;
	}
	
	@Override
	public float calculerSurface() {
		return largeur * hauteur / 2;
	}
	
	//Getters et Setters
	public float getLargeur() {
		return largeur;
	}
	
	public void setLargeur(float largeur) {
		this.largeur = largeur;
	}
	
	public float getHauteur() {
		return hauteur;
	}
	
	public void setHauteur(float hauteur) {
		this.hauteur = hauteur;
	}

	@Override
	public String toString() {
		return "Triangle [largeur=" + largeur + ", hauteur=" + hauteur + "]";
	}


	
	
	
}
