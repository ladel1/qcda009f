package fr.eni.poodrills.modele;

public class Personne {

	private int noPersonne;
	
	private String nom;

	
	@Override
	public boolean equals(Object obj) {
		if(obj==null) return false;	
		Personne p=null;
		if(obj instanceof Personne) 
			p = (Personne) obj;		
		return this.noPersonne==p.getNoPersonne() && this.nom.equals( p.getNom() ) ;
	}
	
	
	public Personne(int noPersonne, String nom) {
		super();
		this.noPersonne = noPersonne;
		this.nom = nom;
	}




	@Override
	public String toString() {
		return "Personne [noPersonne=" + noPersonne + ", nom=" + nom + "]";
	}





	public int getNoPersonne() {
		return noPersonne;
	}




	public void setNoPersonne(int noPersonne) {
		this.noPersonne = noPersonne;
	}




	public String getNom() {
		return nom;
	}




	public void setNom(String nom) {
		this.nom = nom;
	}
	
}
