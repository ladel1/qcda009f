package fr.eni.ecole.bll;

import fr.eni.ecole.bo.Compte;
import fr.eni.ecole.dal.Dao;
import fr.eni.ecole.dal.DaoFactory;

public class CompteManager {
	private Dao<Compte> compteDao;
	//  start Singleton ...........
	// 1. étape 
	private static CompteManager instance = null;
	
	// 2. étape
	private CompteManager() {
		compteDao = DaoFactory.getCompteDao();
	}
	
	// 3.étape
	public static CompteManager getInstance() {
		if(instance==null) {
			instance=new CompteManager();
		}
		return instance;
	}
	// end Singleton ...........
	
	public void crediter(float montant,Compte compte) {
		// validation!!!
		compte.setSolde( compte.getSolde()+montant );
		compteDao.update(compte);
	}
	
	
	public void debiter(float montant,Compte compte) {
		// validation!!!
		compte.setSolde(compte.getSolde()-montant);
		compteDao.update(compte);
	}

	public Compte getCompte(String numCompte) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
