package fr.eni.ecole.bll;

import fr.eni.ecole.bo.Client;
import fr.eni.ecole.dal.Dao;
import fr.eni.ecole.dal.DaoFactory;

// Singleton
public class ClientManager {
	
	private Dao<Client> clientDao;
	
	//  start Singleton ...........
	// 1. étape 
	private static ClientManager instance = null;
	
	// 2. étape
	private ClientManager() {
		clientDao = DaoFactory.getClientDao();
	}
	
	// 3.étape
	public static ClientManager getInstance() {
		if(instance==null) {
			instance=new ClientManager();
		}
		return instance;
	}
	// end Singleton ...........
	
	
	// La logique metier
	
	
	public void CreerNouveauClient(Client client) throws Exception {
		// validation 
		isValid(client);
		// ajouter
		clientDao.insert(client);
	}

	private void isValid(Client client) throws Exception {
		
		if(client==null) throw new Exception("L'objet client ne peut être null!");
		
		if(client.getNom().isBlank()) throw new Exception("Le champs nom est obligatoire!");
		
		if(client.getPrenom().isBlank()) throw new Exception("Le champs nom est obligatoire!");
		
	}
	
	
	
}
