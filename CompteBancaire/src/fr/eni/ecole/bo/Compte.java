package fr.eni.ecole.bo;

public class Compte {
	
	
	private String numCompte;
	private double solde;
	
	private Client client;
	
	
	public Compte() {
		// TODO Auto-generated constructor stub
	}

	
	
	public Compte(String numCompte, double solde,Client client) {
		super();
		this.numCompte = numCompte;
		this.solde = solde;
		this.client = client;
	}



	public Client getClient() {
		return client;
	}



	public void setClient(Client client) {
		this.client = client;
	}



	public String getNumCompte() {
		return numCompte;
	}

	public void setNumCompte(String numCompte) {
		this.numCompte = numCompte;
	}

	public double getSolde() {
		return solde;
	}

	public void setSolde(double solde) {
		this.solde = solde;
	}

	@Override
	public String toString() {
		return "Compte [numCompte=" + numCompte + ", solde=" + solde + "]";
	}
	
	
	
}
