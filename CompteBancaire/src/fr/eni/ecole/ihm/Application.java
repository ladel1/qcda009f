package fr.eni.ecole.ihm;

import java.util.Scanner;

import fr.eni.ecole.bll.CompteManager;
import fr.eni.ecole.bo.Compte;

public class Application {

	private static Scanner sc = new Scanner(System.in);
	
	private static void boot() {
		System.out.println("*******************************************************************");
		System.out.println("************   Application: Compte Bancaire ENI    ***************");
		System.out.println("******************************************************************");
		System.out.println("*** Si vous voulez créer un compte entrez: 1");
		System.out.println("*** Si vous voulez créditer votre compte entrez: 2");
		System.out.println("*** Si vous voulez débiter votre compte entrez: 3");
		System.out.println("******************************************************************");
		
		int choix = sc.nextInt();
		
		switch (choix) {
			case 1: {
				/// ajout compte
				break;
			}
			case 2: {
				System.out.println("Entrez votre numéro de compte");
				 sc.nextLine();
				 String numCompte = sc.nextLine();
				CompteManager compteManager = CompteManager.getInstance();
				System.out.println("Entrez votre montant ?");
				float montant = sc.nextFloat();
				Compte compte = compteManager.getCompte(numCompte);
				compteManager.crediter(montant, compte);
				System.out.println("Votre compte a été bien crédité");
				break;
			}
			case 3: {
				
				break;
			}
			default:
				System.out.println("Erreur de choix");
		}
		

	}
	
	
	
	
	public static void main(String[] args) {
		boot();		
	}
	
}
