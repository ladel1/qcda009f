package fr.eni.ecole.dal;

import fr.eni.ecole.bo.Client;
import fr.eni.ecole.bo.Compte;

public abstract class DaoFactory {

	public static Dao<Client> getClientDao() {
		return new ClientDaoImpl();
	}
	
	public static Dao<Compte> getCompteDao() {
		return new CompteDaoImpl();
	}
	
	
	
}
