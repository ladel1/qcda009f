package fr.eni.ecole.dal;

import java.util.List;

public interface Dao<T> {

	void insert(T t);
	void update(T t);
	List<T> selectAll();
	T selectById(int id);
	void delete(int id);
	void delete(T t);
}
