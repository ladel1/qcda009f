import java.util.Scanner;

public class Main {

	private static Scanner sc = new Scanner(System.in);
	
	public static int factorielle(int n) {
		int f = 1;
		for (int i = 1; i <= n; i++) {
			f = f * i;
		}
		return f;
	}
	
	public static void main(String[] args) {
		
		
//		System.out.println("Entrez un nombre pour calculer le factorielle:");
//		int nombre = sc.nextInt();
//		System.out.println("Factorielle %d est: %d".formatted(nombre,factorielle(nombre)));
	}

}
