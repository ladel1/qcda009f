package demoIOC.bll;

import demoIOC.dal.BI;

public class J {
	private BI bi;
	
	public J(BI bi) {
		this.bi = bi;
	}
	
	public void traitement() {
		bi.onABesoinDeB();
	}
}
