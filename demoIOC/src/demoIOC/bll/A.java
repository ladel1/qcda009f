package demoIOC.bll;

import demoIOC.dal.BI;

public class A {

	private BI bi;
	
	public A(BI bi) {
		this.bi = bi;
	}
	
	public void traitement() {
		bi.onABesoinDeB();
	}
	
}
