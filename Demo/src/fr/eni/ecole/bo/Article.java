package fr.eni.ecole.bo;

public class Article implements Comparable<Article>{

	private String nom;
	private String description;
	private Float prix;
	
	public Article(String nom, String description, float prix) {
		this.nom = nom;
		this.description = description;
		this.prix = prix;
	}
	
	@Override
	public String toString() {
		return "Article [nom=" + nom + ", description=" + description + ", prix=" + prix + "]";
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Float getPrix() {
		return prix;
	}

	public void setPrix(float prix) {
		this.prix = prix;
	}

	@Override
	public int compareTo(Article o) {		
		return this.prix.compareTo(o.getPrix());
	}
	
	
	
}
