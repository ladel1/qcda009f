package fr.eni.ecole.bo;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Main {
	
	static class SortByPrice implements Comparator<Article>{

		@Override
		public int compare(Article o1, Article o2) {
			return o1.getPrix().compareTo(o2.getPrix());
		}
		
	}

	public static void main(String[] args) {
		
		List<Article> articles = new ArrayList<Article>();
		
		articles.add(new Article("Samsung S21 PRO MAX EXTRA ULTRA", "blablab", 1200));
		articles.add(new Article("Iphone 15", "blablab", 1500));
		articles.add(new Article("Ecran Samsung", "blablab", 200));
		articles.add(new Article("Laptop Dell", "blablab", 600));
		articles.add(new Article("NOKIA 3310", "blablab", 30));
		
//		Collections.sort(articles, new Comparator<Article>() {
//
//			@Override
//			public int compare(Article o1, Article o2) {				
//				return o1.getPrix().compareTo(o2.getPrix());
//			}
//			
//		} );
		
		articles.sort(null);
		System.out.println(articles);
		
		
	}

}
