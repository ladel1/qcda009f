import java.lang.reflect.Array;
import java.util.Arrays;

public class Main {

	private static void sort(int [] tab) { // passage par référence
		
		for(int i = 0; i<tab.length-1;i++) {
			for(int j=i+1;j<tab.length;j++) {
				if(tab[i]>tab[j]) {
					int temp = tab[i];
					tab[i] = tab[j];
					tab[j] = temp;
				}
			}
		}
		
	}
	public static void main(String[] args) {
		int []t = {8,4,5,1,3,9,5,4};
		sort(t);
		System.out.println(Arrays.toString(t));
	}

}
