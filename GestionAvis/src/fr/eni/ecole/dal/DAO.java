package fr.eni.ecole.dal;

import java.util.List;

public interface DAO<T> {

	// crud
	default void insert(T t) {}
	default void update(T t){}
	default T selectById(int id){return null;}
	default List<T> selectAll(){return null;}
	default void delete(int id){}	
}
