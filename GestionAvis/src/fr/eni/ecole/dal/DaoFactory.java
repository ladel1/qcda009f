package fr.eni.ecole.dal;

import fr.eni.ecole.bo.Auteur;
import fr.eni.ecole.bo.Avis;

public abstract class DaoFactory {

	public static DAO<Avis> getAvisDao() {
		return new AvisDaoImpl();
	}
	
	public static AuteurDao getAuteurDao() {
		return new AuteurDaoImpl();
	}
	
}
