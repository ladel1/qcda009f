package fr.eni.ecole.dal;

import java.util.List;

import fr.eni.ecole.bo.Auteur;

public class AuteurDaoImpl implements AuteurDao {

	@Override
	public List<Auteur> selectAll() {
		return List.of(new Auteur("Depont","Eric") );
	}

	@Override
	public Auteur selectAuteurByPrenom(String prenom) {
		// TODO Auto-generated method stub
		return new Auteur("Duval","Pierre");
	}
	
}
