package fr.eni.ecole.dal;

import fr.eni.ecole.bo.Auteur;

public interface AuteurDao extends DAO<Auteur> {

	Auteur selectAuteurByPrenom(String prenom);
	
	
	
}
