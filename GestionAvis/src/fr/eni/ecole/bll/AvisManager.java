package fr.eni.ecole.bll;

import fr.eni.ecole.bo.Avis;
import fr.eni.ecole.dal.DAO;
import fr.eni.ecole.dal.DaoFactory;
/**
 * ********* BLL ************
 */
public class AvisManager {

	
	private DAO<Avis> avisDao = DaoFactory.getAvisDao();
	
	// Injection de dépéndence
//	public AvisManager(AvisDao avisDao) {
//		this.avisDao = avisDao;
//	}
	
	public void ajouter(Avis avis) {
		// validation !!!!!!!!!!!!!!!!!
		
		avisDao.insert(avis);
	}
	
}
