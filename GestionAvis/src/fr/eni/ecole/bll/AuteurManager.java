package fr.eni.ecole.bll;

import java.util.List;

import fr.eni.ecole.bo.Auteur;
import fr.eni.ecole.dal.AuteurDao;
import fr.eni.ecole.dal.DAO;
import fr.eni.ecole.dal.DaoFactory;

public class AuteurManager {

	AuteurDao auteurDao = DaoFactory.getAuteurDao();
	
	public List<Auteur> getAll(){
		return auteurDao.selectAll();
	}
	
	public Auteur getOne() {
		return auteurDao.selectAuteurByPrenom("eric");
	}
	
}
