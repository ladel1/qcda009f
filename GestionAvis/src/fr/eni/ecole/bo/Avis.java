package fr.eni.ecole.bo;

import java.time.LocalDate;

public class Avis {

	private int id;
	private String contenu;
	private String auteur;
	private int note;// 0-5
	private LocalDate dateCreation;
	
	public Avis(int id, String contenu, String auteur, int note, LocalDate dateCreation) {
		super();
		this.id = id;
		this.contenu = contenu;
		this.auteur = auteur;
		this.note = note;
		this.dateCreation = dateCreation;
	}

	@Override
	public String toString() {
		return "Avis [id=" + id + ", contenu=" + contenu + ", auteur=" + auteur + ", note=" + note + ", dateCreation="
				+ dateCreation + "]";
	}
}
