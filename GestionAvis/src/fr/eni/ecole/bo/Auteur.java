package fr.eni.ecole.bo;

public class Auteur {

	private String nom;
	private String prenom;
	
	public Auteur() {
		// TODO Auto-generated constructor stub
	}

	public Auteur(String nom, String prenom) {
		super();
		this.nom = nom;
		this.prenom = prenom;
	}

	@Override
	public String toString() {
		return "Auteur [nom=" + nom + ", prenom=" + prenom + "]";
	}
	
	
	
}
