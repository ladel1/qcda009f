package fr.eni.ecole.ihm;

import java.time.LocalDate;
import java.util.Scanner;

import fr.eni.ecole.bll.AvisManager;
import fr.eni.ecole.bo.Avis;

/**
 * ******** IHM *******
 * La couche de présentation <IHM>
 */
public class Main {
	
	private static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		System.out.println("Entrez vous avis:");
		System.out.println("Entrez la note:");
		int note = sc.nextInt();
		sc.nextLine();
		System.out.println("Entrez votre avis:");
		String contenu = sc.nextLine();
		System.out.println("Entrez votre pseudo:");
		String auteur = sc.nextLine();
		LocalDate dateCreation = LocalDate.now();
		/******* creation de l'objet avis */
		int id = 1;
		Avis avis = new Avis(id, contenu, auteur, note, dateCreation);
		
		// appeler le AvisManager de BLL
		AvisManager avisManager = new AvisManager();
		avisManager.ajouter(avis);
		
		
	}

}
