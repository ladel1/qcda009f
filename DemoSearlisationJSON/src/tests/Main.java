package tests;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.exc.StreamWriteException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;

import bo.Burger;

public class Main {

	
	public static void serialiser(String filname,Burger burger ) throws StreamWriteException, DatabindException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.writeValue(new File(filname), burger);

	}
	
	public static void main(String[] args) {
		
		Burger bigMac = new Burger("BigMac");
		
		try {
			serialiser("burger.json",bigMac);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
