package bo;

import java.io.Serializable;

public class Burger implements Serializable {

	private String type;

	public Burger(String type) {
		super();
		this.type = type;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj==null) return false;
		Burger burger = (Burger) obj;
		return this.type.equals(burger.getType());
	}

	@Override
	public String toString() {
		return "Burger [type=" + type + "]";
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
