
public class Main {

	public static void update(int a) { // passage par valeur: int,float,boolean,long,string
		a = 15;
	}
	
	public static void update2(int [] tab) { // passage par référence
		tab[0] = 15;
	}
	
	public static void main(String[] args) {
		int [] h = {0};
		update2(h);
		System.out.println(h[0]);

	}

}
